#include "turoperator.h"
#include <utility>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include "opplegg.h"
#include "LesData3.h"
using namespace std;

/**
 * @brief Construct a new Turoperator object
 *
 * This constructor initializes a Turoperator object using provided details.
 *
 * @param gateAdresse The gate address of the Turoperator
 * @param postNrOgSted The postal code and city of the Turoperator
 * @param navn The name of the Turoperator
 * @param mailAdresse The email address of the Turoperator
 * @param website The website of the Turoperator
 * @param telNr The telephone number of the Turoperator
 * @param antallOpplegg The number of arrangements by the Turoperator
 */
Turoperator::Turoperator(string gateAdresse, string postNrOgSted, string navn,
                         string mailAdresse, string website,
                         long telNr, int antOpplegg) {
    this -> gateAdresse = std::move(gateAdresse);
    this -> postNrOgSted = std::move(postNrOgSted);
    this -> navn = std::move(navn);
    this -> mailAdresse = std::move(mailAdresse);
    this -> website = std::move(website);
    this -> telNr = telNr;
    this -> antallOpplegg = antOpplegg;
}

/**
 * @brief Outputs information about the Turoperator object
 *
 * This function provides the functionality to output (print) details of
 * the Turoperator object. Namely name, antallOpplegg, and website.
 */
void Turoperator::skrivUt() {
    cout << navn << ", " << antallOpplegg << ", " << website << endl;
}

/**
 * @brief Modifies the data of the Turoperator object
 *
 * This function allows the modification of a Turoperator's data. It is designed to
 * interactively ask the user for new data.
 */
void Turoperator::modifyData() {
    cout << "what to modify?\n A - Address, P - Post and Place, N - Name, "
            "M - Mail\n W - Website, T- TelNr, Q - Exit" << endl;
    char chara = lesChar("Enter char: ");
    while (chara != 'Q') {
        switch(chara) {
            case 'A':
                getline(cin, gateAdresse);
                break;
            case 'P':
                getline(cin, postNrOgSted);
                break;
            case 'N':
                getline(cin, navn);
                break;
            case 'M':
                getline(cin, mailAdresse);
                break;
            case 'W':
                getline(cin, website);
                break;
            case 'T':
                cin >> telNr;
                break;
            default:
                break;
        }
        chara = lesChar("Enter char: ");
    }
}

/**
 * @brief Removes the Turoperator object and deals with associated resources
 *
 * @param filename A fstream to the file associated with the Turoperator object.
 *
 * This function is responsible for deleting a Turoperator and removing its data
 * from a file.
 */
void Turoperator::del(string filename) {
    fstream file(filename, fstream::out | fstream::in | fstream::trunc);
    if (!file.good()) {
        cerr << "Failed to open the file." << endl;
        return;
    }
    vector<string> lines;
    string l;
    while (getline(file, l)) {
        stringstream ss(l);
        string name;
        ss >> name;
        if (name != navn) {
            lines.push_back(l);
        } else {
            for (int i=0; i<antallOpplegg; i++){
                string opplegg;
                getline(file, opplegg);
                Opplegg::del("data/" + opplegg);
            }
        }
    }

    for (auto& line : lines) {
        file << line << endl;
    }
}
/**
 * @brief increments antall opplegg.
 * **/
void Turoperator::addOpplegg() {
    antallOpplegg++;
}

int Turoperator::getAntall() const {
    return antallOpplegg;
}
