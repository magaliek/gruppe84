#pragma once

#include <string>
#include "by.h"
#include "attraksjon.h"
#include <vector>
#include <list>
#include <memory>
using namespace std;

struct day {
    shared_ptr<By> city;
    int dagNr;
};

class Opplegg {
private:
    int duration;
    string description;
    vector<shared_ptr<day>> days;
    vector<shared_ptr<string>> itineraries;
    int antItineraries;
public:
    Opplegg(int duration, string description);
    Opplegg();
    string skrivAlt();
    static void del(const string& filename);
    void createItinerary(int i);
    static void printToFile(fstream &file, string whatToPrint);
};