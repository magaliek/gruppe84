/**
 *   Enkel verkt�ykasse for � lese:  tegn og tall.
 *
 *   FULLSTENDIG IDENTISK til 'LesData2.h' bare at adskilt
 *   funksjonenes DEKLARASJON (som er p� DENNE filen) og DEFINISJON.
 *
 *   @file     LesData3.H
 *   @author   Frode Haug, NTNU
 */


#ifndef __LESDATA3_H
#define __LESDATA3_H


const int  MAXCHAR = 200;      //  Max.tegn i input-buffer.


char  lesChar(const char* t);
float lesFloat(const char* t, const float min, const float max);
int   lesInt(const char* t, const int min, const int max);
long long lesLong(const char *t, const long long min, const long long max); //to store phone number
#endif