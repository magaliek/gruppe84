
#include "kirke.h"

Kirke::Kirke(string ID, string beskrivelse, string adresse,
             string website, Type type, int byggAar, int kapasitet) :
        Attraksjon(ID, beskrivelse, adresse, website) {
    this->type = type;
    this->byggAar = byggAar;
    this->kapasitet = kapasitet;
}

string Kirke::skrivUt(string sub) {
    auto pt = ID + ": " + beskrivelse + ", " + std::move(sub) + ", " + adresse + ", "
              + website + ", " + to_string(type) + ", bygg aar: " +
              to_string(byggAar) + ", kapasitet: " + to_string(kapasitet) + '\n';
    return pt;
}

string Kirke::getID() {
    return ID;
}

string Kirke::getType() {
    string type = "Kirke";
    return type;
}
