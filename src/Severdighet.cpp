
#include "severdighet.h"

Severdighet::Severdighet(string ID, string beskrivelse, string adresse, string website, string specialty) :
        Attraksjon(ID, beskrivelse, adresse, website) {
    this->specialty = specialty;
}

string Severdighet::skrivUt(string type) {
    auto pt = ID + ": " + beskrivelse + ", " + std::move(type) + ", " + adresse + ", "
              + website + ", " + specialty + '\n';
    return pt;
}

string Severdighet::getID() {
    return ID;
}

string Severdighet::getType() {
    string type = "Severdighet";
    return type;
}
