#include "funksjoner.h"
#include <iostream>
using namespace std;

void skrivMeny() {
    std::cout << "T: Turoperatorer \n\t- A: skriver ut alle turoperatorene sitt data\n\t- 1: skriver ut data til en turoperator\n\t- N: en ny unik turoperators data leses inn\n\t- E: endre paa en turoperators data \n\t- F: fjern en turoperator \n\t- O: lag ett opplegg" << std::endl;
    std::cout << "B: Byer \n\t- A: skriver ut alle byene sitt data\n\t- 1: skriver ut data til en by\n\t- N: en ny unik bys data leses inn\n\t- E: endre paa en bys data \n\t- F: fjern en by" << std::endl;
    std::cout << "A: Attraksjoner \n\t- N: en ny attraksjon legges inn i datastrukturen\n\t- F: fjerner en attraksjon" << std::endl;
}