#pragma once

#include <string>
#include "attraksjon.h"
using namespace std;

class Severdighet : public Attraksjon {
private:
    string ID, beskrivelse, adresse, website, specialty;
public:
    Severdighet(string ID, string beskrivelse, string adresse, string website, string specialty);
    string skrivUt(string type) override;
    string getID() override;
    string getType();
};
