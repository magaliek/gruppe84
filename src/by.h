#pragma once

#include <list>
#include <memory>
#include "attraksjon.h"
using namespace std;

class By {
private:
    string city;
    string country;
    list<shared_ptr<Attraksjon>> attraksjoner;
public:
    By(string city, string country);
    list<shared_ptr<Attraksjon>> getAttractions();
    void del(string filename);
    string getCountry();
    string getCity();
    void nyAttraksjon(string& filename);
    void fjernAttraksjon(string& filename);
};
