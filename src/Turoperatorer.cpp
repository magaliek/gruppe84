#include "turoperatorer.h"
#include "opplegg.h"
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <memory>
#include <iostream>
#include <iomanip>
#include <vector>
#include "LesData3.h"
using namespace std;

/**
 * @brief Construct a new Turoperatorer object from an input file stream
 *
 * This constructor initializes a Turoperatorer object using data read from an input file stream.
 * It expects the file stream to be already open and correctly positioned before being passed to the constructor.
 *
 * @param in The input file stream from which to read the data
 */
Turoperatorer::Turoperatorer() {

};

void Turoperatorer::readFromFile(fstream& in) {
    if (!in.good()) {
        cerr << "Failed to open the file." << endl;
        return;  // exit if failed to open the file
    }
    string gateAdresse, postNrOgSted, navn, mailAdresse, website;
    long telNr;
    int antOpplegg;
    string ignoreOpplegg;
    string line;
    //TUI 0 opplegg, 789 Travelways Street, 54321 Tourville, 
    //contact@tui.com, www.tui.com, 1234567890
    while (getline(in, line)) {
        stringstream s(line);
        string navn;
        s >> navn;
        s >> antOpplegg;
        s >> ignoreOpplegg;
        s.ignore(2);
        getline(s, gateAdresse, ',');
        s.ignore(1);
        getline(s, postNrOgSted, ',');
        s.ignore(1);
        getline(s, mailAdresse, ',');
        s >> website;
        s.ignore(2);
        s >> telNr;
        operators[navn] = make_shared<Turoperator>(gateAdresse, postNrOgSted, navn, mailAdresse, website, telNr, antOpplegg);
    }
}
/**
 * Skriver ut på skjermen alle turoperatrene sitt navn,
 * antall nåværende opplegg og hjemmeside-adressen.
 * En turoperatør pr.linje.
 * */
void Turoperatorer::skrivAlle() {
    for (auto& operatorPair : operators) {
        operatorPair.second -> skrivUt();
    }
}

/**
 * @brief Retrieves a Turoperator object based on their name
 * @param navn - The name of the Turoperator to retrieve
 * @return A unique pointer to the Turoperator object, or nullptr if not found
 */
shared_ptr<Turoperator> Turoperatorer::getOperator(string& navn) {
    auto opp = operators.find(navn);
    if (opp != operators.end()) {
        return opp->second;
    } else {
        cerr << "Invalid name" <<endl;
        return nullptr;
    }
}


shared_ptr<Turoperator> Turoperatorer::newOperator(string fileName, fstream& file) {
    string name;
    cout << "Enter name: ";
    getline(cin, name);
    if (operators.find(name) != operators.end()) {
        cerr << "Operator already exists" << endl;
        return nullptr;
    }
    string addr;
    cout << "Enter address: ";
    cin.ignore();
    getline(cin, addr);
    string postOgSted;
    cout << "Enter post and place: ";
    cin.ignore();
    getline(cin, postOgSted);
    string email;
    cout << "Enter email: ";
    cin.ignore();
    getline(cin, email);
    string website;
    cout << "Enter website: ";
    cin.ignore();
    getline(cin, website);
    long long tlf = lesLong("Enter phone number: ", 1000000000, 9999999999);
    auto newOpp = make_shared<Turoperator>(std::move(addr),
                                           std::move(postOgSted),
                                           std::move(name), std::move(email),
                                           std::move(website), tlf);
    operators[name] = newOpp;

    //append file
    file.open(std::move(fileName), std::ios_base::app);
    if (file.is_open()) {
        file << name << " " << newOpp->getAntall() << " opplegg, " << addr << ", "
             << postOgSted << ", " << email << ", " << website <<
             ", " << to_string(tlf) << endl;
        file.close();
    } else {
        cerr << "Error opening file" << endl;
    }
    return newOpp;
}

/**
 * @brief Modifies an existing Turoperator object based on their name
 * @param name - The name of the Turoperator to modify
 */
void Turoperatorer::modifyOperator() {
    string name;
    cout << "Enter name: ";
    getline(cin, name);
    getOperator(name) -> modifyData();
}

/**
 * @brief Removes a Turoperator object from the collection
 * @param turoperator - A shared pointer to the Turoperator to be removed
 */
void Turoperatorer::del(string filename) {
    string name;
    cout << "Enter name: ";
    getline(cin, name);
    auto opp = getOperator(name);
    if (opp == nullptr) {
        return;
    }
    char sure = lesChar(("Are you sure you want to delete" + name + "? y/n").c_str());
    if (sure == 'Y') {
        getOperator(name) -> del(filename);
        operators.erase(name);
    } else {
        cout << "canceled";
    }
}

/**
 * @brief This is the editing process for a tour program. An operator name is first inputted.
 * Following that, a general description of the planned itinerary is entered. After that, the number of days (1-30)
 * that the program will last is entered.
 *
 * For each day, the program then asks for a unique city/site name.
 * All of the attractions in said city/site are then displayed for the user.
 * The user can then select attractions to add to this day until they are happy.
 * The user can then either choose to continue with the same day (maybe adding a new city)
 * or proceed to the next day. This process then repeats, asking for cities/sites and attractions,
 * until either the entire itinerary has been inputted or the user decides to quit early.
 *
 * After this, the program displays a summary on the screen - the number of days, the cities/sites for each day,
 * and the attractions for each city/site. also everything is added to file.
 * The user is then asked if they want to save this itinerary to a file. If they say yes, the summary is written to a file.
 * The file's name has a specific format:   - <001-...> -  dg.dta. The number 001-... is generated from a counter
 * (antOpplegg) that counts the number of itineraries created by this operator.
 *
 * @param duration
 * @param file
 * **/
void Turoperatorer::lagOpplegg(string turFileName) {
    //make file
    fstream in(turFileName, std::ios::out | std::ios::trunc);
    if (!in.good()) {
        cerr << "Failed to open the file." << endl;
        return;
    }

    //get opplegg data from user
    string name;
    cout << "Enter name of operator: ";
    cin >> name;
    getOperator(name)->addOpplegg();
    int antOpplegg = getOperator(name)->getAntall();
    string description;
    cout << "Enter a general description of the opplegg" << endl;
    cin.ignore(); //ignore newline
    getline(cin, description);
    cin.ignore(); //ignore newline
    int duration = lesInt("Enter duration (min. 1 day, max. 30 days): ", 1, 30);

    //making the actual opplegg
    Opplegg oppleggObj(duration, description);
    cout << "Summery: " << endl;
    cout << oppleggObj.skrivAlt() << endl;

    //print summery to file
    char answer = lesChar("Write to file? (y/n) ");
    if (answer == 'Y') {
        //make file
        stringstream ss;
        ss << std::setw(3) << std::setfill('0') << antOpplegg;
        string filename = name + "-" + ss.str() + "-" + to_string(duration) + "DG.DTA";
        //making the actual file
        fstream opp(filename);
        //write to file
        Opplegg::printToFile(opp, "summery: \n"+oppleggObj.skrivAlt()+'\n');

        //adding the filename to TUROPERATORER.DTA
        vector<string> lines;
        string line;
        while (getline(in, line)) {
            stringstream s(line);
            string navn;
            s >> navn;
            if (navn != name) {
                lines.push_back(line);
            } else {
                lines.push_back(line);
                lines.push_back(filename);
            }
        }
        for (auto& l : lines) {
            in << l << endl;
        }
    } else {
        cout << "No file created" << endl;
    }
}
