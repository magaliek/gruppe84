#pragma once

#include <map>
#include <string>
#include <fstream>
using namespace std;

class Turoperator {
private:
    string gateAdresse, postNrOgSted, navn, mailAdresse, website;
    int telNr, antallOpplegg;
public:
    Turoperator(string gateAdresse, string postNrOgSted, string navn, string mailAdresse,
                string website, long telNr, int antOpplegg = 0); // newOperator() calls constructor
    void skrivUt(); //in skrivAlle()
    void modifyData();
    void del(string filename);
    void addOpplegg();
    int getAntall() const;
};