
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include "enumer.h"
#include "by.h"
#include "opplegg.h"
#include "LesData3.h"
#include "museumGalleri.h"
#include "kirke.h"
#include "severdighet.h"

using namespace std;

By::By(string city, string country) {
    this->city = city;
    this->country = country;
}

list<shared_ptr<Attraksjon>> By::getAttractions() {
    return attraksjoner;
}

string By::getCountry() {
    return country;
}

string By::getCity() {
    return city;
}

void By::del(string filename) {
    fstream in(filename, fstream::out | fstream::in | fstream::trunc);
    if (!in.good()) {
        cerr << "Failed to open the file." << endl;
        return;
    }
    vector<string> lines;
    string l;
    while (getline(in, l)) {
        stringstream ss(l);
        string name;
        ss >> name;
        if (name != city) {
            lines.push_back(l);
        } else {
            for (int i=0; i<attraksjoner.size(); i++){
                string attr;
                getline(in, attr); //skip
            }
        }
    }

    for (auto& line : lines) {
        in << line << endl;
    }
}

void By::nyAttraksjon(string& filename) {
    ofstream out(filename, ios::app);
    if (!out.is_open()) {
        cerr << "Failed to open file for writing." << endl;
        return;
    }

    string typeName;
    char type;
    type = lesChar("Enter type (M - museum, K - Kirke, S - Severdighet");
    shared_ptr<Attraksjon> newAttr;
    if (type == 'M') {
        typeName = "Museum Gallery";
        string ID;
        cout << "Enter the ID: ";
        getline(cin, ID);
        string beskrivelse;
        cout << "Enter the description: ";
        getline(cin, beskrivelse);
        string adresse;
        cout << "Enter the address: ";
        getline(cin, adresse);
        string website;
        cout << "Enter the website: ";
        getline(cin, website);
        string hoydepunkt;
        cout << "Enter the highlight: ";
        getline(cin, hoydepunkt);
        newAttr = make_shared<MuseumGalleri>(ID, beskrivelse, adresse, website, hoydepunkt);

    } else if (type == 'K') {
        typeName = "Church";
        string ID, beskrivelse, adresse, website;
        Type type;
        int byggAar, kapasitet;

        cout << "Enter the ID: ";
        getline(cin, ID);

        cout << "Enter the description: ";
        getline(cin, beskrivelse);

        cout << "Enter the address: ";
        getline(cin, adresse);

        cout << "Enter the website: ";
        getline(cin, website);

        int enumType = lesInt("Enter the Kirke type (0 for katedral, 1 for kirke, 2 for kapell): ", 0, 2);

        type = static_cast<Type>(enumType);

        byggAar = lesInt("Enter the Build Year: ", 0, 9999);
        kapasitet = lesInt("Enter the Capacity: ", 0, 10000);

        newAttr = make_shared<Kirke>(ID, beskrivelse, adresse, website, type, byggAar, kapasitet);

    } else if (type == 'S') {
        typeName = "Severdighet"; //idk what this is in english
        string ID, beskrivelse, adresse, website, specialty;
        cout << "Enter the ID: ";
        getline(cin, ID);

        cout << "Enter the description: ";
        getline(cin, beskrivelse);

        cout << "Enter the address: ";
        getline(cin, adresse);

        cout << "Enter the website: ";
        getline(cin, website);

        cout << "Enter the specialty: ";
        getline(cin, specialty);

        newAttr = make_shared<Severdighet>(ID, beskrivelse, adresse, website, specialty);

    } else {
        cout << "Invalid input";
        return;
    }
    attraksjoner.push_back(newAttr);
    //add to file
    if (newAttr) {
        out << newAttr->skrivUt(typeName);
    }
    out.close();

}

void By::fjernAttraksjon(string &filename) {
    ifstream in(filename);
    if (!in.is_open()) {
        cerr << "Failed to open the file for reading." << endl;
        return;
    }

    int i=0;
    for (auto& attr : attraksjoner) {
        i++;
        cout << to_string(i) + ": " + attr->skrivUt(attr->getType());
    }
    int index= lesInt("Velg nummeret p� attraksjonen som skal fjernes: ", 1, getAttractions().size());

    // 1-based to 0-based
    index--;
    auto it = attraksjoner.begin();
    std::advance(it, index); // Move the iterator to the correct position

    auto at = *it;
    if (it != attraksjoner.end()) {
        attraksjoner.erase(it);
    }
    //remove from file
    vector<string> lines;
    string line;

    while (getline(in, line)) {
        stringstream ss(line);
        string currentID;
        getline(ss, currentID, ':');

        if (currentID != at->getID()) {
            lines.push_back(line);
        }
    }

    in.close();

}

