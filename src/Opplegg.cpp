#include <fstream>
#include "opplegg.h"
#include "LesData3.h"
#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

/**
 * @brief Constructs an Opplegg object
 * @param duration The duration of the tour
 * @param description The description of the tour
 */
Opplegg::Opplegg(int duration, string description) {
    this->duration = duration;
    this->description = description;
    auto quitChara = ' ';
    for (int i=0; i<duration; i++) {
        if (quitChara != 'Q') {
            createItinerary(i);
            quitChara = lesChar("Quit early? (Q) N-no");
        } else {
            return;
        }
    }
    this->antItineraries=0;
}

/**
 * @brief Creates itinerary for a day
 * Et entydig turoperat�rnavn skrives.
 * Deretter leses en generell beskrivelse av det
 * forest�ende opplegget som n� skal skrives inn.
 * Deretter skrives antall dager (1-30) opplegget varer.
 * (*) Deretter skrives �Dag nr.N�, og aktuelt entydig navn p�
 * by/sted leses inn. S� vises alleattraksjonene p� dette stedet/byen.
 * Brukeren velger og legger til attraksjoner inntil hen er forn�yd.
 * Deretter velger brukeren � fortsette med samme (�S�) dag (og kanskje
 * en ny by/sted), eller � g� til neste (�N�) dag. Alt dette gjentar seg
 * (fra *-merket) for byer/steder og attraksjoner inntil man er ferdig med
 * siste dag og siste attraksjon den dagen, eller om man �nsker � avslutte
 * (�Q�) alt f�r dette.
 * beskrivelsen, dagnumrene,  og for hver dag:  by-/stednavn
 * og attraksjonene i byen(e)/sted(ene)
 * @param i The day number
 */
void Opplegg::createItinerary(int i) {
    shared_ptr<day> newDay = make_shared<day>();
    days.push_back(newDay);
    newDay->dagNr = i + 1;
    string cityName;
    string countryName;
    auto itinerary = make_shared<string>(std::move(description) + " Day "
                                         + to_string(newDay->dagNr) + ": ");
    char continueAdding= 'S';
    while (continueAdding == 'S') {
        cout << "Enter city name" << endl;
        getline(cin, cityName);
        cout << "which country is the city in?" << endl;
        getline(cin, countryName);
        cout << "Enter description" << endl;
        getline(cin, description);
        *itinerary += std::move(cityName) + '\n'
                      + "Attractions: ";
        auto city = make_shared<By>(cityName, countryName);
        newDay->city = city;
        for (auto attr: newDay->city->getAttractions()) {
            string t = attr->getType();
            cout << attr->skrivUt(t);
            auto chara = lesChar("Add this attraction to city? y/n");
            if (chara == 'Y') {
                *itinerary += attr->skrivUt(t);
            }
            if (attr == newDay->city->getAttractions().back()) {
                cout << "last attraction ";
                return;
            }
        }

        continueAdding = lesChar("Continue with this day? (Add another city/attraction) S(same day)/N(next day)");
        if (continueAdding == 'N') {
            itineraries.push_back(itinerary);
            return;
        }
    }
}

/**
* @brief Default constructor for Opplegg
*/
Opplegg::Opplegg() {}

/**
 * @brief Returns a summary of the tour
 * Writes duration, day, city, and attractions for each day
 * beskrivelsen, dagnumrene,  og for hver dag:  by-/stednavn
 * og attraksjonene i byen(e)/sted(ene)
 * @return A string summary of the tour
 */
string Opplegg::skrivAlt() {
    string summery;
    for (auto itinerary : itineraries) {
        summery += *itinerary + '\n';
    }
    return summery;
}
/**
 * @brief Deletes file with given filename
 * @param filename Name of the file to be deleted
 */
void Opplegg::del(const string& filename) {
    remove(("data/" + filename).c_str());
}

/**
 * @brief Prints tour summary to file
 * @param file File stream to print the summary to
 */
void Opplegg::printToFile(fstream &file, string whatToPrint) {
    file << std::move(whatToPrint) << endl;
}
