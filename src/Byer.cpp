#include <iostream>
#include <utility>
#include <fstream>
#include <sstream>
#include <cctype>
#include "byer.h"
#include "enumer.h"
#include "LesData3.h"
#include "severdighet.h"
#include "museumGalleri.h"
#include "kirke.h"

using namespace std;

/**
* @brief Default constructor.
*/
Byer::Byer() {

}

void Byer::readFromFile(fstream& in) {
    string city, country;
    string ID, beskrivelse, adresse, website, type;
    //MILANO, Italia
    //AmazingChurch: the church is a historical religious building with a lot of history,
    //chruch, 123 churchStreet, www.church.com, Katedral, bygg aar: 1000, kapasitet: 200
    string line;
    while (getline(in, line)) {
        stringstream s(line);
        s >> city;
        s.ignore(2); //ignore comma
        getline(s, country);
        for (auto attr : byer[city]->getAttractions()) {
            string line;
            getline(in, line);
            stringstream ss(line);
            ss >> ID;
            ss.ignore(2);
            getline(ss, beskrivelse, ',');
            getline(ss, type, ',');
            getline(ss, adresse, ',');
            getline(ss, website, ',');
            //check subclass of attraction
            if (tolower(type[0]) == 'm') {
                string hoydepunkt;
                getline(ss, hoydepunkt);
                auto museum = make_shared<MuseumGalleri>(ID, beskrivelse, adresse, website, hoydepunkt);
                byer[city]->getAttractions().push_back(museum);
            } else if (tolower(type[0]) == 'k') {
                int byggAar, kapasitet;
                Type ttype;
                string tttype;
                getline(ss, tttype, ',');
                if (tolower(tttype[2])=='t') {
                    ttype = Type::katedral;
                } else if (tolower(tttype[2])=='r') {
                    ttype = Type::kirke;
                } else if (tolower(tttype[2])== 'p') {
                    ttype = Type::kapell;
                }
                ss >> byggAar;
                ss.ignore(2);
                ss >> kapasitet;
                auto kirke = make_shared<Kirke>(ID, beskrivelse, adresse, website, ttype, byggAar, kapasitet);
                byer[city]->getAttractions().push_back(kirke);
            } else if (tolower(type[0]) == 's') {
                string specialty;
                getline(ss, specialty);
                auto severdighet = make_shared<Severdighet>(ID, beskrivelse, adresse, website, specialty);
                byer[city]->getAttractions().push_back(severdighet);
            }
        }
    }

}
/**
 * @brief Skriver ut på skjermen alle byenes/stedenes navn,
 * landet det ligger i og antall attraksjoner i byen/på stedet.
 * Alt på en linje pr.by/sted.
 */
void Byer::skrivAlle() {
    for (auto by : byer) {
        cout << by.second->getCity() << ", " << by.second->getCountry()
             << ", " << by.second->getAttractions().size() << " attractions." << endl;
    }
}

/**
 * @brief Prints details about a specified city.
 * Etter at brukeren har skrevet et entydig by-/stedsnavn,
 * skrives antall attraksjoner ut,  og alt om alle attraksjoner
 * i byen/på stedet, inkludert tekstene: «Museum/Galleri», «Kirke»
 * eller «Severdighet». Det er vel naturlig med 3-4 linjer med utskrift pr.attraksjon.
 */
void Byer::en() { //add the type of attraction
    string cityName;
    string type;
    cout << "Enter city name:" << endl;
    getline(cin, cityName);
    auto city = getCity(cityName);
    if (city == nullptr) {
        return;
    } else {
        string all = to_string(city->getAttractions().size()) + ": ";
        for (auto attr : city->getAttractions()) {
            //check what kind of attraction it is
            shared_ptr<MuseumGalleri> museum = dynamic_pointer_cast<MuseumGalleri>(attr);
            shared_ptr<Kirke> kirke = dynamic_pointer_cast<Kirke>(attr);
            shared_ptr<Severdighet> severdighet = dynamic_pointer_cast<Severdighet>(attr);

            if (museum) {
                type = "Museum Gallery";
                all += attr->skrivUt(type);
            } else if (kirke) {
                type = "Church";
                all += attr->skrivUt(type);
            } else if (severdighet) {
                type = "Severdighet";
                all += attr->skrivUt(type);
            }
        }
        cout << all << endl;
    }
}




void Byer::ny() {
    string city;
    cout << "Enter name of city to add: ";
    getline(cin, city);
    if (byer.find(city) != byer.end()) {
        cerr << "City already exists" << endl;
        return;
    }
    cout << "Enter country the city is in: ";
    cin.ignore();
    string country;
    getline(cin, country);
    auto newCity = make_shared<By>(std::move(city), std::move(country));
    byer[city] = newCity;
}

/**
 * @brief Removes the city with the given name from the collection.
 * @param name The name of the city to be removed.
 */
void Byer::fjern(string filename) {
    string city;
    cout << "Enter city: ";
    getline(cin, city);
    auto by = getCity(city);
    if (by == nullptr) {
        cout << "No such city";
        return;
    }
    char sure = lesChar(("Are you sure you want to delete" + city + "? y/n").c_str());
    if (sure == 'Y') {
        getCity(city) -> del(filename);
        byer.erase(city);
    } else {
        cout << "canceled";
    }
}

/**
 * @brief Fetches a city with the given name from the collection.
 * @param name The name of the city to fetch.
 * @return A shared pointer to the fetched city; null if the city does not exist.
 */
shared_ptr<By> Byer::getCity(string &name) {
    if (byer.find(name) != byer.end()) {
        return byer[name];
    } else {
        cerr << "city isn't in itinerary" << endl;
        return nullptr;
    }
}

void Byer::addAttr(string& filename) {
    string name;
    cout << "which city to add attraction to? ";
    getline(cin, name);
    getCity(name)->nyAttraksjon(filename);
}

void Byer::delAttr(string& filename) {
    string name;
    cout << "which city to delete attraction from? ";
    getline(cin, name);
    getCity(name)->fjernAttraksjon(filename);
}

