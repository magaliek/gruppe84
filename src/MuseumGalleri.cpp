
#include "museumGalleri.h"

MuseumGalleri::MuseumGalleri(string ID, string beskrivelse, string adresse,
                             string website, string hoydepunkt) : Attraksjon(ID, beskrivelse,
                                                                             adresse, website) {
    this->hoydePunkt = hoydepunkt;
}

string MuseumGalleri::skrivUt(string type) {
    auto pt = ID + ": " + beskrivelse + ", " + std::move(type) + ", " + adresse + ", "
              + website + ", " + hoydePunkt + '\n';
    return pt;
}

string MuseumGalleri::getID() {
    return ID;
}

string MuseumGalleri::getType() {
    string type = "MuseumGalleri";
    return type;
}

