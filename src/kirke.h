#pragma once

#include <string>
#include "attraksjon.h"
#include "enumer.h"
using namespace std;

class Kirke : public Attraksjon {
private:
    int byggAar, kapasitet;
    string ID, beskrivelse, adresse, website;
    Type type;
public:
    Kirke(string ID, string beskrivelse, string adresse,
          string website, Type type, int byggAar, int kapasitet);
    string skrivUt(string type) override;
    string getID() override;
    string getType();
};