#pragma once

#include <string>
#include "attraksjon.h"
using namespace std;

class MuseumGalleri : public Attraksjon {
private:
    string ID, beskrivelse, adresse, website, hoydePunkt;
public:
    MuseumGalleri(string ID, string beskrivelse, string adresse,
                  string website, string hoydepunkt);
    string skrivUt(string type) override;
    string getID() override;
    string getType();
};

