#pragma once

#include <map>
#include <string>
#include "turoperator.h"
#include <memory>
#include <fstream>
using namespace std;

class Turoperatorer {
private:
    map<string, shared_ptr<Turoperator>> operators;
public:
    explicit Turoperatorer();
    void skrivAlle();
    shared_ptr<Turoperator> getOperator(string& navn); //getOperator(string name).skrivUt()
    shared_ptr<Turoperator> newOperator(string filename, fstream& file); // calls Turoperator constructor
    void modifyOperator(); //getOperator(string name).modifyData()
    void del(string filename); // calls del(ifstream in) in Turoperator and also in deconstructor
    void lagOpplegg(string turFileName);
    void readFromFile(fstream& in);
};

