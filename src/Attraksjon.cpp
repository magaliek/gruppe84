#include <string>
#include <memory>
#include "attraksjon.h"
using namespace std;

Attraksjon::Attraksjon(string ID, string beskrivelse, string adresse, string website) {
    this->ID = ID;
    this->beskrivelse = beskrivelse;
    this->adresse = adresse;
    this->website = website;
}
