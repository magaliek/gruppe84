#pragma once
#include <string>
#include <memory>

using namespace std;

class Attraksjon {
private:
    string ID, beskrivelse, adresse, website; //ID is name
public:
    Attraksjon(string ID, string beskrivelse, string adresse, string website);
    virtual string skrivUt(string type)=0;
    virtual string getID()=0;
    virtual string getType()=0;
};