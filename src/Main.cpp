/**
 *   Hovedprogrammet for OOP-prosjektet V24 med turoperatorer, byer,
 *   attraksjoner i byene og N dagers ulike reiseopplegg for turoperatorene.
 *
 *   @file     MAIN.CPP
 *   @author   Magalie Robin Kalash, NTNU
 */


#include <iostream>
#include <list>
#include "turoperatorer.h"
#include "byer.h"
#include "opplegg.h"
#include "funksjoner.h"
#include "LesData3.h"
#include <clocale>
using namespace std;


Turoperatorer gTuroperatorer;
Byer gByer;
int main()  {
    setlocale(LC_ALL, "C");
    skrivMeny();

    string turoperatorerFilename = "../src/TUROPERATORER.DTA";
    fstream turoperatorerIn(turoperatorerFilename);
    gTuroperatorer.readFromFile(turoperatorerIn);

    string byerFilename = "../src/BYER.DTA";
    fstream byerIn(byerFilename);
    gByer.readFromFile(byerIn);

    char input = lesChar("Enter T, B or A. Q to quit");

    if (input != 'A' && input != 'B' && input != 'T') {
        if (input == 'Q') {
            cout<<"you quit";
            return 0;
        }
        cerr << "invalid input";
    }
    string name;
    while (input != 'Q') {
        switch(input){
            case 'T':
                skrivMeny();
                input = lesChar("Enter A, 1, N, E, F, O, or Q to quit");
                switch(input){
                    case 'A':
                        gTuroperatorer.skrivAlle();
                        break;
                    case '1':
                        cout << "Enter name: ";
                        cin >> name;
                        gTuroperatorer.getOperator(name);
                        break;
                    case 'N':

                        gTuroperatorer.newOperator(turoperatorerFilename, turoperatorerIn);
                        break;
                    case 'E':
                        gTuroperatorer.modifyOperator();
                        break;
                    case 'F':
                        gTuroperatorer.del(turoperatorerFilename);
                        break;
                    case 'O':
                        gTuroperatorer.lagOpplegg(turoperatorerFilename);
                        break;
                    case 'Q':
                        cout << "You have chosen to quit";
                        return 0;
                    default:
                        cerr << "invalid input" << endl;
                        break;
                }
                break;
            case 'B':
                skrivMeny();
                input = lesChar("A, 1, N, F, Q-quit");
                switch(input) {
                    case 'A':
                        gByer.skrivAlle();
                        break;
                    case '1':
                        gByer.en();
                        break;
                    case 'N':
                        gByer.ny();
                        break;
                    case 'F':
                        gByer.fjern(byerFilename);
                        break;
                    case 'Q':
                        cout << "quitting...";
                        return 0;
                    default:
                        cerr << "Invalid input" << endl;
                        break;
                }
                break;
            case 'A':
                skrivMeny();
                input = lesChar("N, F, Q-quit");
                switch(input) {
                    case 'N':
                        gByer.addAttr(byerFilename);
                        break;
                    case 'F':
                        gByer.delAttr(byerFilename);
                        break;
                    case 'Q':
                        cout << "quitting...";
                        return 0;
                    default:
                        cerr << "Invalid input" << endl;
                        break;
                }
                break;
            default:
                cerr << "invalid input" << endl;
                break;
        }
    }

}
