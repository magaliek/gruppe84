#pragma once

#include <map>
#include <string>
#include "by.h"
#include <memory>
using namespace std;

class Byer {
private:
    map<string, shared_ptr<By>> byer;
public:
    Byer();
    void skrivAlle();
    void en();
    void ny();
    void fjern(string filename);
    shared_ptr<By> getCity(string& name);
    void addAttr(string& filename);
    void delAttr(string& filename);
    void readFromFile(fstream& in);
};
